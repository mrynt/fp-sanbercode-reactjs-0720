import React from 'react';
import { Navigate } from 'react-router-dom';
import DashboardLayout from 'src/layouts/DashboardLayout';
import MainLayout from 'src/layouts/MainLayout';
import DashboardView from 'src/views/dashboard';
import LoginView from 'src/views/auth/LoginView';
import NotFoundView from 'src/views/errors/NotFoundView';
import RegisterView from 'src/views/auth/RegisterView';
import Movielist from 'src/views/movielist';
import Movieform from 'src/views/movielist/MovieForm';
import Gameform from 'src/views/gamelist/GameForm';
import Gamelist from 'src/views/gamelist';
import Game from 'src/views/game';
import Movie from 'src/views/movie';

const routes = [
  {
    path: '/',
    element: <MainLayout />,
    children: [
      { path: 'login', element: <LoginView /> },
      { path: 'register', element: <RegisterView /> },
      { path: '404', element: <NotFoundView /> },
      { path: '/', element: <Navigate to="/login" /> },
      { path: '*', element: <Navigate to="/404" /> }
    ]
  },
  {
    path: 'app',
    element: <DashboardLayout />,
    children: [
      { path: 'dashboard', element: <DashboardView /> },
      { path: 'movie', element: <Movie /> },
      { path: 'game', element: <Game /> },
      { path: 'movielist', element: <Movielist /> },
      { path: 'movielist/add', element: <Movieform /> },
      { path: 'movielist/edit/:id', element: <Movieform /> },
      { path: 'gamelist/add', element: <Gameform /> },
      { path: 'gamelist/edit/:id', element: <Gameform /> },
      { path: 'gamelist', element: <Gamelist /> },
      { path: '*', element: <Navigate to="/404" /> }
    ]
  }
];

export default routes;
