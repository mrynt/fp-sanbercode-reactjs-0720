import React, {useEffect,useState} from "react"
import axios from "axios";
import { useParams , useNavigate } from 'react-router-dom';
import {
    Box,
    Container,
    TextField,
    Button

  } from '@material-ui/core';


const MovieFrom = (props) =>{
      const { id } = useParams();
      const[datamovie,setdatamovie] = useState(null)
      const [title, settitle]  =  useState("")
      const [description, setdescription]  =  useState("")
      const [year, setyear]  =  useState("")
      const [duration, setduration]  =  useState("")
      const [genre, setgenre]  =  useState("")
      const [rating, setrating]  =  useState("")
      const [image_url, setimage_url]  =  useState("")

      const navigate = useNavigate();


  
        if(datamovie === null){
            axios.get('https://backendexample.sanbersy.com/api/movies/'+id)
            .then(res => {
                setdatamovie(res.data)
                settitle(res.data.title)
                setdescription(res.data.description)
                setyear(res.data.year)
                setduration(res.data.duration)
                setgenre(res.data.genre)
                setrating(res.data.rating)
                setimage_url(res.data.image_url)

            })
        }
       
 

      const handleInputForm = (event) =>{
        console.log(event.target.name);

        if(event.target.name === 'title'){
            settitle(event.target.value)
        }

        if(event.target.name === 'description'){
            setdescription(event.target.value)
        }

        if(event.target.name === 'year'){
            setyear(event.target.value)
        }

        if(event.target.name === 'duration'){
            setduration(event.target.value)
        }

        if(event.target.name === 'genre'){
            setgenre(event.target.value)
        }

        if(event.target.name === 'rating'){
            setrating(event.target.value)
        }

        if(event.target.name === 'image_url'){
            setimage_url(event.target.value)
        }

      }
      

      

    

    const handleSubmit = (event) =>{
        event.preventDefault()

        if(id  === undefined){
            axios.post('https://backendexample.sanbersy.com/api/movies', {title:title,
                                                                            description:description,
                                                                            year:year,
                                                                            duration:duration,
                                                                            genre:genre,
                                                                            rating:rating,
                                                                            image_url:image_url
                                                                            })
            .then(res => {
                navigate('/app/movielist', { replace: true });
            })
        }else{
            console.log(id)
            axios.put('https://backendexample.sanbersy.com/api/movies/'+id, {
                                                                        id:id,
                                                                        title:title,
                                                                        description:description,
                                                                        year:year,
                                                                        duration:duration,
                                                                        genre:genre,
                                                                        rating:rating,
                                                                        image_url:image_url,
                                                                        
                                                                        })
            .then(res => {
                navigate('/app/movielist', { replace: true });
            })
        }
        
        
      

        settitle("")
        setdescription("")
        setyear("")
        setduration("")
        setgenre("")
        setrating("")
      }

  return(

      <Container maxWidth={false}>
            <Box mt={3}>
      <h1>Form Movie</h1>
        <form onSubmit={handleSubmit}>
          <div>
                 
          <TextField    label="Masukkan nama Film"
                        margin="normal"
                        type="text" 
                        fullWidth
                        name="title" value={title} onChange={handleInputForm}/>
          </div>
          <div>
           <TextField   label="Masukkan Description Film"
                        margin="normal"
                        fullWidth
                        type="text" name="description" value={description} onChange={handleInputForm}/>
          </div>
          <div>
      
          <TextField    label="Masukkan Tahun Film"
                        margin="normal"
                        fullWidth
                        type="number" placeholder="YYYY" min="1945" max="2100" format name="year"  value={year} onChange={handleInputForm}/>
          </div>

          <div>
          <TextField label="Masukkan Durasi Film"
                        margin="normal"
                        fullWidth
                        type="number" name="duration"  value={duration} onChange={handleInputForm}/>
          </div>

          <div>
          <TextField label="Masukkan Genre Film"
                        margin="normal"
                        fullWidth
                        type="number"type="text" name="genre"  value={genre} onChange={handleInputForm}/>
          </div>

          <div>
         
          <TextField label="Masukkan Rating Film"
                        margin="normal"
                        fullWidth
                        type="number" min="0" max="10"  name="rating"  value={rating} onChange={handleInputForm}/>
          </div>

          <div>
          <TextField label="Masukkan Image URL Film"
                        margin="normal"
                        fullWidth
                        type="text" name="image_url"  value={image_url} onChange={handleInputForm}/>
          </div>
          
          <Button color="warning" size="large"
                    variant="contained"  type="submit">submit</Button>
        </form>
        </Box>
        </Container>
  )

}

export default MovieFrom