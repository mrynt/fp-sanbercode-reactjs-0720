import React, { useState, createContext,useEffect } from "react";
import axios from "axios";
export const MovieContext = createContext();

export const MovieProvider = props => {
    const [movielist, setmovielist] =  useState(null)
    const [daftarfilm, setdaftarfilm] =  useState(null)
    const [titles, settitle]  =  useState("")
    const [descriptions, setdescription]  =  useState("")
    const [years, setyear]  =  useState("")
    const [durations, setduration]  =  useState("")
    const [genres, setgenre]  =  useState("")
    const [ratings, setrating]  =  useState("")
    const [indexOfForm, setIndexOfForm] =  useState(-1)
    const [id, setid] =  useState(-1) 
    const [count, setcount] =  useState(0)


    useEffect( () => {
        if(movielist ===null){
            axios.get('https://backendexample.sanbersy.com/api/movies')
            .then(res => {
                setmovielist(res.data.map(el => {return {id:el.id,
                                                        title:el.title,
                                                        year:el.year,
                                                        review:el.review,
                                                        rating:el.rating,
                                                        image_url:el.image_url,
                                                        genre:el.genre,
                                                        duration:el.duration,
                                                        description:el.description,
                                                    }}))
                                                    setcount(res.data.length)                                    
            })
        }
       
      })

      return (
        <MovieContext.Provider value={{value1:[movielist, setmovielist],
          value2:[indexOfForm, setIndexOfForm],
          value3:[id, setid],
          value4:[count, setcount]
                                                }}>
          {props.children}
        </MovieContext.Provider>
      );
};