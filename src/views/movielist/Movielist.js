import React, {useContext,useState} from "react"
import axios from "axios";
import {MovieContext} from "./MovieContext"
import { Link as RouterLink, useNavigate } from 'react-router-dom';
import PerfectScrollbar from 'react-perfect-scrollbar';
import Page from 'src/components/Page';

import { Search as SearchIcon } from 'react-feather';
import {
  Box,
  Button,
  Card,
  Container,
  CardContent,
  Table,
  TextField,
  InputAdornment,
  SvgIcon,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableSortLabel,
  TableRow,
  makeStyles
} from '@material-ui/core';


const useStyles = makeStyles((theme) => ({
    root: {
      backgroundColor: theme.palette.background.dark,
      minHeight: '100%',
      paddingBottom: theme.spacing(3),
      paddingTop: theme.spacing(3)
    }
  }));
  

const Movielist = ({ className}) =>{
    const { value1,value4} = React.useContext(MovieContext);  
    const [movielist,setmovielist] = value1
    const [count, setcount] = value4;
    const [limit, setLimit] = useState(10);
    const [page, setPage] = useState(0);
    const classes = useStyles();
    const [search, setsearch] = useState('');
    const [order, setOrder] = React.useState('asc');
    const [orderBy, setOrderBy] = React.useState('no');
    const handleLimitChange = (event) => {
        setLimit(event.target.value);
    };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  const navigate = useNavigate();


  const handleDelete = (event) =>{
    let index = event.target.value;
    let indexarr = event.target.getAttribute('data-indexarr')
    let newmovie = movielist
    axios.delete('https://backendexample.sanbersy.com/api/movies/'+index)
    .then(res => {
      newmovie.splice(indexarr,1)
        setmovielist([...newmovie])
    })
    
  } 

  const handlesearch = (event) =>{
    console.log(event.target.name);
    console.log(event.target.value);
    setsearch(event.target.value);
    return event.target.value;
     
  }

  const createSortHandler = (property) => (event) => {
    if(order === 'asc'){

      if(property == 'title'){
        let newmovie = movielist.sort((a,b) => (a.title > b.title) ? 1 : ((b.title > a.title) ? -1 : 0));
        setmovielist([...newmovie])
  
      }
      
     
      if(property == 'year'){
        let newmovie = movielist.sort((a,b) => (a.year > b.year) ? 1 : ((b.year > a.year) ? -1 : 0));
        setmovielist([...newmovie])
 
      }

      if(property == 'duration'){
        let newmovie = movielist.sort((a,b) => (a.duration > b.duration) ? 1 : ((b.duration > a.duration) ? -1 : 0));
        setmovielist([...newmovie])
 
      }
      
      setOrder('desc')
    }else{
      if(property == 'title'){
        let newmovie = movielist.sort((a,b) => (a.title < b.title) ? 1 : ((b.title < a.title) ? -1 : 0));
        setmovielist([...newmovie])
      }
      
      if(property == 'year'){
        let newmovie = movielist.sort((a,b) => (a.year < b.year) ? 1 : ((b.year < a.year) ? -1 : 0));
        setmovielist([...newmovie])
      }

      if(property == 'duration'){
        let newmovie = movielist.sort((a,b) => (a.duration < b.duration) ? 1 : ((b.duration < a.duration) ? -1 : 0));
        setmovielist([...newmovie])
      }
      
      setOrder('asc')
    }

  };

    
  return(
    <Page
    className={classes.root}
    title="Movie"
  >
    <Container maxWidth={false}>

    <Box
    display="flex"
    justifyContent="flex-end"
  >
    <Button
      color="primary"
      variant="contained"
      onClick={() => {
        navigate('/app/movielist/add', { replace: true });
   }}
    >
      Add Movie
    </Button>
  </Box>

  <Box mt={3}>
        <Card>
          <CardContent>
            <Box maxWidth={500}>
              <TextField
              value={search}
              onChange={handlesearch}
                fullWidth
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <SvgIcon
                        fontSize="small"
                        color="action"
                      >
                        <SearchIcon />
                      </SvgIcon>
                    </InputAdornment>
                  )
                }}
                placeholder="Search Movie By Title"
                variant="outlined"
              />
            </Box>
          </CardContent>
        </Card>
      </Box>

  <Card>
    <PerfectScrollbar>
      <Box minWidth={1050}>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>
                No
              </TableCell>
              <TableCell>
                Title
                <TableSortLabel
                  onClick={createSortHandler('title')}
                >
                </TableSortLabel>
              </TableCell>
              <TableCell>
                Description
              </TableCell>
              <TableCell>
                Year
                <TableSortLabel
                  onClick={createSortHandler('year')}
                >
                </TableSortLabel>
              </TableCell>
              <TableCell>
                Duration
                <TableSortLabel
                  onClick={createSortHandler('duration')}
                >
                </TableSortLabel>
              </TableCell>
              <TableCell>
                Genre
              </TableCell>
              <TableCell>
                Rating
              </TableCell>
              <TableCell>
                Action
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
          {movielist !== null && movielist.slice(0, limit).filter(movielist => movielist.title.toLowerCase().includes(search)).map((val,index)=> {
                    return (
                        <TableRow key={index}> 
                            <TableCell>{index+1}</TableCell>
                            <TableCell>{val.title}</TableCell>
                            <TableCell>{val.description}</TableCell>
                            <TableCell>{val.year}</TableCell>
                            <TableCell>{val.duration}</TableCell>
                            <TableCell>{val.genre}</TableCell>
                            <TableCell>{val.rating}</TableCell>
                            <TableCell>
                                <Button onClick={() => {
                                          navigate('/app/movielist/edit/'+val.id+'', { replace: true });
                                              }} color="secondary" variant="contained" value={val.id} data-indexarr={index}>Edit</Button>
                                &nbsp;
                                <Button onClick={handleDelete} variant="contained" color="" value={val.id} data-indexarr={index}>Delete</Button>
                            </TableCell>
                        </TableRow>
                    )
                    })}
          </TableBody>
        </Table>
      </Box>
    </PerfectScrollbar>
    <TablePagination
      component="div"
      count={count}
      onChangePage={handlePageChange}
      onChangeRowsPerPage={handleLimitChange}
      page={page}
      rowsPerPage={limit}
      rowsPerPageOptions={[5, 10, 25]}
    />
  </Card>
  </Container>
  </Page>
  )

}


export default Movielist