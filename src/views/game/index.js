import React, { useState,useEffect } from 'react';
import axios from "axios";
import {
  Box,
  Container,
  Grid,
  makeStyles
} from '@material-ui/core';
import Page from 'src/components/Page';
import ProductCard from './ProductCard';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  },
  productCard: {
    height: '100%'
  }
}));

const Game = () => {
  const classes = useStyles();
  const [gamelist,setGamelist] = useState(null);

  useEffect( () => {
    if(gamelist ===null){
        axios.get('https://backendexample.sanbersy.com/api/games')
        .then(res => {
          setGamelist(res.data.map(el => {return {id:el.id,
            name:el.name,
            platform:el.platform,
            release:el.release,
            singlePlayer:el.singlePlayer,
            multiplayer:el.multiplayer,
            genre:el.genre,
            image_url:el.image_url
          }}))
        })
    }
   
  })

  console.log(gamelist);

  return (
    <Page
      className={classes.root}
      title="Products"
    >
      <Container maxWidth={false}>
        <Box mt={3}>
          <Grid
            container
            spacing={3}
          >
            {gamelist !== null && gamelist.map((val,index)=> {
                    return (
                      <Grid
                      item
                      key={val.id}
                      lg={4}
                      md={6}
                      xs={12}
                    >
                      <ProductCard
                        className={classes.productCard}
                        product={val}
                      />
                    </Grid>
                    )
                    })}
        
          </Grid>
        </Box>
        <Box
          mt={3}
          display="flex"
          justifyContent="center"
        >
        </Box>
      </Container>
    </Page>
  );
};

export default Game;
