import axios from "axios";

const API_URL = "https://backendexample.sanbersy.com/api/";

class AuthService {
  login(username, password) {
    return axios
      .post(API_URL + "login", {
        username,
        password
      })
      .then(response => {

        if (response.data.id) {
          localStorage.setItem("username", JSON.stringify(response.data.username));
        }

        return response.data;
      });
  }

  logout() {
    localStorage.removeItem("username");
  }


  getCurrentUser() {
    return JSON.parse(localStorage.getItem('username'));
  }
}

export default new AuthService();