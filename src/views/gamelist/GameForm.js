import React, {useEffect,useState} from "react"
import axios from "axios";
import { useParams , useNavigate } from 'react-router-dom';
import {
    Box,
    Container,
    TextField,
    Button,
    Select,
    MenuItem,
    InputLabel

  } from '@material-ui/core';


const GameForm = (props) =>{
      const { id } = useParams();
      const[datagame,setdatagame] = useState(null)
      const [name, setname]  =  useState("")
      const [platform, setplatform]  =  useState("")
      const [release, setrelease]  =  useState("")
      const [singlePlayer, setsinglePlayer]  =  useState("")
      const [multiplayer, setmultiplayer]  =  useState("")
      const [genre, setgenre]  =  useState("")
      const [image_url, setimage_url]  =  useState("")

      const navigate = useNavigate();


  
        if(datagame === null){
            axios.get('https://backendexample.sanbersy.com/api/games/'+id)
            .then(res => {
                setdatagame(res.data)
                setname(res.data.name)
                setplatform(res.data.platform)
                setrelease(res.data.release)
                setsinglePlayer(res.data.singlePlayer)
                setmultiplayer(res.data.multiplayer)
                setgenre(res.data.genre)
                setimage_url(res.data.image_url)

            })
        }
       
 

      const handleInputForm = (event) =>{
        console.log(event.target.name);

        if(event.target.name === 'name'){
            setname(event.target.value)
        }

        if(event.target.name === 'platform'){
            setplatform(event.target.value)
        }

        if(event.target.name === 'release'){
            setrelease(event.target.value)
        }

        if(event.target.name === 'singlePlayer'){
            setsinglePlayer(event.target.value)
        }

        if(event.target.name === 'multiplayer'){
            setmultiplayer(event.target.value)
        }

        if(event.target.name === 'genre'){
            setgenre(event.target.value)
        }

        if(event.target.name === 'image_url'){
            setimage_url(event.target.value)
        }

      }
      

      

    

    const handleSubmit = (event) =>{
        event.preventDefault()

        if(id  === undefined){
            axios.post('https://backendexample.sanbersy.com/api/games', {name:name,
                                                                            platform:platform,
                                                                            release:release,
                                                                            singlePlayer:singlePlayer,
                                                                            multiplayer:multiplayer,
                                                                            genre:genre,
                                                                            image_url:image_url
                                                                            })
            .then(res => {
                navigate('/app/gamelist', { replace: true });
            })
        }else{
            console.log(id)
            axios.put('https://backendexample.sanbersy.com/api/games/'+id, {
                                                                        id:id,
                                                                        name:name,
                                                                        platform:platform,
                                                                        release:release,
                                                                        singlePlayer:singlePlayer,
                                                                        multiplayer:multiplayer,
                                                                        genre:genre,
                                                                        image_url:image_url,
                                                                        
                                                                        })
            .then(res => {
                navigate('/app/gamelist', { replace: true });
            })
        }
        
        
      

        setname("")
        setplatform("")
        setrelease("")
        setsinglePlayer("")
        setmultiplayer("")
        setgenre("")
        setimage_url("")
      }

  return(

      <Container maxWidth={false}>
            <Box mt={3}>
      <h1>Form Movie</h1>
        <form onSubmit={handleSubmit}>
          <div>
                 
          <TextField    label="Masukkan nama Game"
                        margin="normal"
                        type="text" 
                        fullWidth
                        name="name" value={name} onChange={handleInputForm}/>
          </div>
          <div>
           <TextField   label="Masukkan Platform"
                        margin="normal"
                        fullWidth
                        type="text" name="platform" value={platform} onChange={handleInputForm}/>
          </div>
          <div>
      
          <TextField    label="Masukkan Release Game"
                        margin="normal"
                        fullWidth
                        type="number" placeholder="YYYY" min="1945" max="2100" format name="release"  value={release} onChange={handleInputForm}/>
          </div>
          <div style={{marginTop:"15px"}}>
           <InputLabel id="demo-simple-select">Single Player ?</InputLabel> 
          <Select
          name="singlePlayer"
          fullWidth
          label="Single Player ?"
          id="demo-simple-select"
          value={singlePlayer}
          onChange={handleInputForm}
        >
          <MenuItem value={0}>No</MenuItem>
          <MenuItem value={1}>Yes</MenuItem>
        </Select>
        </div>
        <div style={{marginTop:"15px"}}>
        <InputLabel id="demo-simple-selects">Multi Player ?</InputLabel> 
        <Select
            name="multiplayer"
          fullWidth
          label="Multi Player ?"
          id="demo-simple-selects"
          value={multiplayer}
          onChange={handleInputForm}
        >
          <MenuItem value={0}>No</MenuItem>
          <MenuItem value={1}>Yes</MenuItem>
        </Select>
        </div>
          
            <br />
          <div>
         
          <TextField label="Masukkan Genre Game"
                        margin="normal"
                        fullWidth
                        type="text" name="genre"  value={genre} onChange={handleInputForm}/>
          </div>

          <div>
          <TextField label="Masukkan Image URL Game"
                        margin="normal"
                        fullWidth
                        type="text" name="image_url"  value={image_url} onChange={handleInputForm}/>
          </div>
          
          <Button color="warning" size="large"
                    variant="contained"  type="submit">submit</Button>
        </form>
        </Box>
        </Container>
  )

}

export default GameForm