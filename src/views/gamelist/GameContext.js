import React, { useState, createContext,useEffect } from "react";
import axios from "axios";
export const GameContext = createContext();

export const GameProvider = props => {
    const [gamelist, setgamelist] =  useState(null)
    const [indexOfForm, setIndexOfForm] =  useState(-1)
    const [id, setid] =  useState(-1) 
    const [count, setcount] =  useState(0)


    useEffect( () => {
        if(gamelist ===null){
            axios.get('https://backendexample.sanbersy.com/api/games')
            .then(res => {
              setgamelist(res.data.map(el => {return {id:el.id,
                                                        name:el.name,
                                                        platform:el.platform,
                                                        release:el.release,
                                                        singlePlayer:el.singlePlayer,
                                                        multiplayer:el.multiplayer,
                                                        genre:el.genre
                                                    }}))
              setcount(res.data.length)
            })
        }
       
      })

      return (
        <GameContext.Provider value={{value1:[gamelist, setgamelist],
          value2:[indexOfForm, setIndexOfForm],
          value3:[id, setid],
          value4:[count, setcount]
                                                }}>
          {props.children}
        </GameContext.Provider>
      );
};