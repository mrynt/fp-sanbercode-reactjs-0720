import React, {useContext,useState} from "react"

import {GameContext} from "./GameContext"
import axios from "axios";
import { Link as RouterLink, useNavigate } from 'react-router-dom';
import PerfectScrollbar from 'react-perfect-scrollbar';
import Page from 'src/components/Page';
import { Search as SearchIcon } from 'react-feather';
import {
  Box,
  Button,
  Card,
  Container,
  CardContent,
  Table,
  TextField,
  InputAdornment,
  SvgIcon,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableSortLabel,
  TableRow,
  makeStyles
} from '@material-ui/core';


const useStyles = makeStyles((theme) => ({
    root: {
      backgroundColor: theme.palette.background.dark,
      minHeight: '100%',
      paddingBottom: theme.spacing(3),
      paddingTop: theme.spacing(3)
    }
  }));
  

const Gamelist = ({ className}) =>{
    const { value1,value4} = React.useContext(GameContext);  
    const [gamelist,setgamelist] = value1
    const [limit, setLimit] = useState(10);
    const [page, setPage] = useState(0);
    const [count, setcount] = value4;
    const classes = useStyles();
    const [order, setOrder] = React.useState('asc');
    const [orderBy, setOrderBy] = React.useState('no');
    const [search, setsearch] = useState('');
    const handleLimitChange = (event) => {
        setLimit(event.target.value);
    };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };



  const navigate = useNavigate();

  const handleDelete = (event) =>{
    let index = event.target.value;
    let indexarr = event.target.getAttribute('data-indexarr')
    let newgame = gamelist
    axios.delete('https://backendexample.sanbersy.com/api/games/'+index)
    .then(res => {
      newgame.splice(indexarr,1)
      setgamelist([...newgame])
    })
    
  } 

  const handlesearch = (event) =>{
    console.log(event.target.name);
    console.log(event.target.value);
    setsearch(event.target.value);
    return event.target.value;
     
  }
  



  const createSortHandler = (property) => (event) => {
    if(order === 'asc'){

      if(property == 'name'){
        let newgame = gamelist.sort((a,b) => (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0));
        setgamelist([...newgame])
  
      }
      
     
      if(property == 'relase'){
        let newgame = gamelist.sort((a,b) => (a.release > b.release) ? 1 : ((b.release > a.release) ? -1 : 0));
        setgamelist([...newgame])
 
      }

      if(property == 'platform'){
        let newgame = gamelist.sort((a,b) => (a.platform > b.platform) ? 1 : ((b.platform > a.platform) ? -1 : 0));
        setgamelist([...newgame])
 
      }
      
      setOrder('desc')
    }else{
      if(property == 'name'){
        let newgame = gamelist.sort((a,b) => (a.name < b.name) ? 1 : ((b.name < a.name) ? -1 : 0));
        setgamelist([...newgame])
      }
      
      if(property == 'relase'){
        let newgame = gamelist.sort((a,b) => (a.release < b.release) ? 1 : ((b.release < a.release) ? -1 : 0));
        setgamelist([...newgame])
      }

      if(property == 'platform'){
        let newgame = gamelist.sort((a,b) => (a.platform < b.platform) ? 1 : ((b.platform < a.platform) ? -1 : 0));
        setgamelist([...newgame])
      }
      
      setOrder('asc')
    }

  };

  return(
    <Page
    className={classes.root}
    title="Game"
  >
    <Container maxWidth={false}>

    <Box
    display="flex"
    justifyContent="flex-end"
  >
    <Button
      color="primary"
      variant="contained"
      onClick={() => {
        navigate('/app/gamelist/add', { replace: true });
   }}
    >
      Add Game
    </Button>
  </Box>

  <Box mt={3}>
        <Card>
          <CardContent>
            <Box maxWidth={500}>
              <TextField
               value={search}
                onChange={handlesearch}
                fullWidth
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <SvgIcon
                        fontSize="small"
                        color="action"
                      >
                        <SearchIcon />
                      </SvgIcon>
                    </InputAdornment>
                  )
                }}
                placeholder="Search Game Name"
                variant="outlined"
              />
            </Box>
          </CardContent>
        </Card>
      </Box>

  <Card>
    <PerfectScrollbar>
      <Box minWidth={1050}>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell sortDirection={orderBy === '1' ? order : false}>
                No
              </TableCell>
              <TableCell>
                Name
                <TableSortLabel
                  onClick={createSortHandler('name')}
                >
                </TableSortLabel>
              </TableCell>
              <TableCell>
                Platform
                <TableSortLabel
                  onClick={createSortHandler('platform')}
                >
                </TableSortLabel>
              </TableCell>
              <TableCell>
                Relase
                <TableSortLabel
                  onClick={createSortHandler('relase')}
                >
                </TableSortLabel>
              </TableCell>
              <TableCell>
                SinglePlayer
              </TableCell>
              <TableCell>
                MultiPlayer
              </TableCell>
              <TableCell>
                Genre
              </TableCell>
              <TableCell>
                Action
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
          {gamelist !== null && gamelist.slice(0, limit).filter(gamelist => gamelist.name.toLowerCase().includes(search)).map((val,index)=> {
                    return (
                        <TableRow key={index}> 
                            <TableCell>{index+1}</TableCell>
                            <TableCell>{val.name}</TableCell>
                            <TableCell>{val.platform}</TableCell>
                            <TableCell>{val.release}</TableCell>
                            
                              
                              <TableCell>{val.singlePlayer == '1' ? 'YES' : 'NO'}</TableCell>
                            
                            
                            
                            <TableCell>{val.multiplayer == '1' ? 'YES' : 'NO'}</TableCell>
                            <TableCell>{val.genre}</TableCell>
                            <TableCell>
                                <Button onClick={() => {
                                          navigate('/app/gamelist/edit/'+val.id+'', { replace: true });
                                              }} color="secondary" variant="contained" value={val.id} data-indexarr={index}>Edit</Button>
                                &nbsp;
                                <Button onClick={handleDelete} variant="contained" color="" value={val.id} data-indexarr={index}>Delete</Button>
                            </TableCell>
                        </TableRow>
                    )
                    })}
          </TableBody>
        </Table>
      </Box>
    </PerfectScrollbar>
    <TablePagination
      component="div"
      count={count}
      onChangePage={handlePageChange}
      onChangeRowsPerPage={handleLimitChange}
      page={page}
      rowsPerPage={limit}
      rowsPerPageOptions={[5, 10, 25]}
    />
  </Card>
  </Container>
  </Page>
  )

}


export default Gamelist