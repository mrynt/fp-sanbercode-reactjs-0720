import React from "react"
import {GameProvider} from "./GameContext"
import Gamelist from "./Gamelist"
// import MovieForm from "./MovieForm"
// import { useAuth } from "./context/auth";

const Game = () =>{


  return(
    <GameProvider>
      <Gamelist/>
    </GameProvider>
  )
}

export default Game